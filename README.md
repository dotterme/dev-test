### What is this repository for? ###

We would like you to build a simple javascript single-product widget that responds to the configuration file (config.js) and can be embedded into an existing web page.  Ordinarily, these would be themable but for this exercise, you may hardcode design elements into the widget.

This repository contains the following files:

* src/config.js
* src/main.js (contains boilerplate to get you started - you can add files as necessary)
* index.html (host page for embedded widget)

The config.js file contains an object with three attributes:

* The product feed URL
* A boolean flag to show/hide the product image
* The image to be used from the JSON feed

The widget should display the first product from the feed (there is only one anyway).  Feel free to be as creative as you like but code quality is the driving factor.

Feed attributes to be displayed in the widget:

* Product image (use `product.images` array in feed)
* Product name `product.attributes.name`
* List of skus where the product is in-stock `product.skus.availability`
* Retailer icon (use desktop logo from feed) `retailers[{retailerName}].desktopLogoURL`
* Price `product.skus.formattedPrice`

Feed URL: http://feeds.dotter.me/2/582c97f6795cb9626350857e/en_gb.json

### Wireframe ###

![mockup](https://s3.amazonaws.com/content.dotter.me/dotter/dev-test/mockup-wireframe.png)

### Contribution guidelines ###

* This document along with a mockup wireframe can be found [here](https://docs.google.com/document/d/1609K0929BY80lARniy_Ah_V5aI0r5TbXoO4GKUq5geg/edit?usp=sharing).
* Pay particural attention to code quality and lint where possible
* The primary focus is on functional programming
* Slick design features are always a bonus but a clean, professional looking UX/UI is a must
* The above wireframe is predominantly mobile in appearance but responsive CSS layouts will earn bonus points

### Who do I talk to? ###

* Evan Summers
* evan.summers@dotter.me