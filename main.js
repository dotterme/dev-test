window.Dotter = function Dotter(config) {
    // this constructor is called from index.html
    // you can access the config through the argument passed into this constructor
    this.config = config;
    console.log(JSON.stringify(config, null, 2));
}

Dotter.prototype.embed = function (container) {
    // this function is called from index.html
    // the container passed into this function is a dom element
    // you have to render your app inside the container

    var p = document.createElement('p');
    p.innerHTML = 'The feed url is ' + this.config.feedURL;
    container.appendChild(p);
}
